# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:slim
EXPOSE 8000

# Keeps Python from generating .pyc files in the container
#ENV PYTHONDONTWRITEBYTECODE=1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

WORKDIR /code
COPY . /code/

# Install pip requirements
RUN pip install -r requirements.txt

VOLUME ["/code/db"]

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000